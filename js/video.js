// Video
function resize() {
    if (($(window).width() != $("video").width())) {
        $("video").width($(window).width());
        $("video").height("auto");
    };
    if ($("video").height() < $(window).height()) {
        $("video").height($(window).height());
        $("video").width("auto");
    };
    $("body").css('padding-top', $(window).height() + 'px')
};
$(document).on('ready', resize);
$(window).on('resize', resize);
$("video").on('loadeddata', resize);

// Buttons
$(document).ready(function() {
    $("#loader").animate({
        opacity: 0
    }, 1000);
    $("#main").animate({
        opacity: 1
    }, 2000);
    $("#begin,#begin-arrow,#begin-topmenu").click(function() {
        $('html, body').animate({
            scrollTop: $("#begin-target").offset().top
        }, 500);
    });
    $("#process-topmenu").click(function() {
        $('html, body').animate({
            scrollTop: $("#process-target").offset().top
        }, 500);
    });
    $("#contacts-topmenu").click(function() {
        $('html, body').animate({
            scrollTop: $("#contacts-target").offset().top
        }, 500);
    });
});
// Header menu
$header = $('.header__fake');

$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll > 20) {
        $header.addClass('animated').removeClass('fix');
        $('.icn__hamburger').fadeIn();
    }
    else if (scroll < 20 & $(window).width() > 768) {
        $header.removeClass('animated').addClass('fix');
        $('.icn__hamburger').fadeOut();
    }
});
$(window).resize(function() {
    if ($(window).scrollTop() < 20 & $(window).width() < 768) {
        $('.icn__hamburger').fadeIn();
    }
    else if ($(window).scrollTop() < 20 & $(window).width() > 768) {
        $header.removeClass('animated').addClass('fix');
        $('.icn__hamburger').fadeOut();
    }
    else if ($(window).scrollTop() >= 20 & $(window).width() > 768) {
        $('.icn__hamburger').fadeIn();
    };
});